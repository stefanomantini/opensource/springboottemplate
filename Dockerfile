#pull base image
FROM openjdk:8-jdk-alpine

#maintainer
MAINTAINER sman6798@gmail.com

#expose port 8080
EXPOSE 8080

#copy hello world to docker image
COPY ./target/template.jar ./app.jar

#default command
CMD java -jar ./app.jar
