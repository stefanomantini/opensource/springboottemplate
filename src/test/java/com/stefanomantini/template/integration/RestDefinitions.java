//package com.stefanomantini.template.integration;
//
//import cucumber.api.PendingException;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import org.junit.Before;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.client.SimpleClientHttpRequestFactory;
//import org.springframework.web.client.RestTemplate;
//
//import java.net.URL;
//
//import static org.junit.Assert.assertTrue;
//
//public class RestDefinitions extends CucumberRoot{
//
//    Logger LOG = LoggerFactory.getLogger(RestDefinitions.class);
//
//    private int port = 8080;
//    private RestTemplate template;
//    private URL base;
//    @Before
//    public void setUp() throws Exception {
//        this.base = new URL("http://localhost:" + port + "/");
//        SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
//        template = new RestTemplate(requestFactory);
//    }
//
//    @Given("The application is running")
//    public void test_is_server_up() {
//        LOG.debug(String.valueOf(template.getForEntity(base+"/health", String.class)));
//        assertTrue(template.getForEntity(base + "/health", String.class).getStatusCode().is2xxSuccessful());
//    }
//
//    @When("^the client requests data from \"([^\"]*)\"$")
//    public ResponseEntity<String> theClientCalls(String arg0) throws Throwable {
//        ResponseEntity<String> health = template.getForEntity(base + "/health", String.class);
//        LOG.debug(health.toString());
//        return health;
//    }
//
//    @Then("^the client receives a status code of \"([^\"]*)\"$")
//    public void theClientReceivesAStatusCodeOf(String arg0) throws Throwable {
//        throw new PendingException();
//    }
//}
