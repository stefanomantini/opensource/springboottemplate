package com.stefanomantini.template.acceptance;

import com.stefanomantini.template.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootApplicationTest {

	@Test
	public void applicationStarts() {
		Application.main(new String[] {});
	}

	@Test
	public void contextLoads() {
	}

}
