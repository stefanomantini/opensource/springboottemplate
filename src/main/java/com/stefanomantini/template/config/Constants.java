package com.stefanomantini.template.config;

public class Constants {

    // Users
    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";

    // Profiles
    public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
    public static final String SPRING_PROFILE_TEST = "test";
    public static final String SPRING_PROFILE_QA = "qa";
    public static final String SPRING_PROFILE_PROD = "prod";

    // Regexes
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    private Constants() {
    }
}
